
window.JQUERY_URL = "https://code.jquery.com/jquery-3.4.1.min.js";

window.CHEAT_JS   = "https://bitbucket.org/fudotsu/geoguessr_tguess/raw/bcc2ece3e5cfd283d39d8f66cb34e3eb80f4fcbc/geoguessr_tguess.js";
window.CHEAT_HTML = "https://bitbucket.org/fudotsu/geoguessr_tguess/raw/bcc2ece3e5cfd283d39d8f66cb34e3eb80f4fcbc/geoguessr_tguess_o.html";
window.CHEAT_CSS  = "https://bitbucket.org/fudotsu/geoguessr_tguess/raw/bcc2ece3e5cfd283d39d8f66cb34e3eb80f4fcbc/geoguessr_tguess.css";

(function() {

    function MakeXMLRequest(url, cb) {
        var xhr = new XMLHttpRequest(); 
        xhr.onreadystatechange = function(){ 
            cb(xhr)
        }.bind(this);
        xhr.open("GET", url, true); 
        xhr.send(); 
    }

    // Inject JQuery
    MakeXMLRequest(window.JQUERY_URL, function(xhr) {
        if(xhr.readyState == 4){
            if(xhr.status == 200 || xhr.status == 0){
                (new Function(xhr.responseText)())

                // Inject cheat JS
                MakeXMLRequest(window.CHEAT_JS, function(xhr) {
                    if(xhr.readyState == 4){
                        if(xhr.status == 200 || xhr.status == 0){
                            (new Function(xhr.responseText)())     
                        }
                    }
                })

                // Inject cheat HTML
                MakeXMLRequest(window.CHEAT_HTML, function(xhr) {
                    if(xhr.readyState == 4){
                        if(xhr.status == 200 || xhr.status == 0){
                            $('#fguess_overlay').remove()
                            $('body').append(xhr.responseText)
                        }
                    }
                })

                // Inject cheat CSS
                MakeXMLRequest(window.CHEAT_CSS, function(xhr) {
                    if(xhr.readyState == 4){
                        if(xhr.status == 200 || xhr.status == 0){
                            $('body').append('<style>' + xhr.responseText + '</style>')            
                        }
                    }
                })
            }
        }
    })

    return ""
})()


