
window.eSum = 0
var oldU;

function getGeoBox() {
    return $('#fguess_geo');
}

function getOverlay() {
    return $('#fguess_overlay');
}

function getRandomSum() {
    return new Date().getMilliseconds();
}

// Checking the URL on map content
function isUrlIsMap(url) {
    return url.startsWith('https://maps.googleapis.com/maps/api/js/GeoPhotoService.GetMetadata');
}

var cheatRun = function() {
    var thisSum = getRandomSum();

    $('#fguess_toggle').click(function(e) {
        if (thisSum != eSum) { return; }

        e.preventDefault()

        var closed = $(this).attr('closed');
        
        if (closed == "1") {
            $('#fguess_geo').css('height', '320px')
            
            $(this).attr('closed', "0")
        }else{
            $('#fguess_geo').css('height', '20px')

            $(this).attr('closed', "1")
        }
    });

    $('head').on("DOMNodeInserted", function(e) {
        if (thisSum != eSum) { return; }

        var el = $(e.target)
        if (!el.attr('src')) { return; }
        if (!isUrlIsMap(el.attr('src'))) { return; }

        var url = el.attr('src')

        if (oldU == url) { return; }

        $.get('https://cors-anywhere.herokuapp.com/' + url, function( data ) {
            var inf = data
            inf = inf.slice(inf.indexOf('['))
            inf= inf.substr(0, inf.indexOf(')'))

            var p = JSON.parse(inf)

            var pos = p[1][0][5][0][1][0]
            var x = pos[2]
            var y = pos[3]

            getGeoBox().html('')
            getGeoBox().append('<div class="mapouter" style="padding-top: 20px;"><div class="gmap_canvas"><iframe width="400" height="300" id="gmap_canvas" src="https://maps.google.com/maps?q=' + x + ' ' + y + '&t=&z=3&ie=UTF8&iwloc=&output=embed&gestureHandling=greedy" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>Google Maps Generator by <a href="https://www.embedgooglemap.net">embedgooglemap.net</a></div><style>.mapouter{position:relative;text-align:right;height:300px;width:400px;}.gmap_canvas {overflow:hidden;background:none!important;height:300px;width:400px;}</style></div>')
        });

        oldU = url
    });

    eSum = thisSum
}

setTimeout(function() {
    cheatRun()
}, 900)

